//
//  Category.swift
//  Todoey
//
//  Created by Dejan Markovic on 1/22/19.
//  Copyright © 2019 Stefana Mladenovic. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name : String = ""
    @objc dynamic var color : String = ""
    let items = List<Item>()
    
}
